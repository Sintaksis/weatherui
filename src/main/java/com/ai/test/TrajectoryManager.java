package com.ai.test;

import javafx.util.Pair;

import java.util.Random;

public class TrajectoryManager {
    public static final int LIMIT_X = 10;
    public static final int LIMIT_Y = 10;
    private static final Random random = new Random();

    public Pair<Integer, Integer> generateNewTrajectory() {
        return new Pair<Integer, Integer>(random.nextInt(3) - 1, random.nextInt(3) - 1);
    }

    public int checkXTrajectory(int oldValue, int delta) {
        int newValue = oldValue + delta;
        return (newValue < LIMIT_X) && newValue >= 0 ? newValue : oldValue;
    }

    public int checkYTrajectory(int oldValue, int delta) {
        int newValue = oldValue + delta;
        return (newValue < LIMIT_Y) && newValue >= 0 ? newValue : oldValue;
    }
}
