package com.ai.test;

import javafx.util.Pair;

import java.util.Random;

import static com.ai.test.TrajectoryManager.LIMIT_X;
import static com.ai.test.TrajectoryManager.LIMIT_Y;

public class World {
    private static final Random random = new Random();
    private static final Object[][] world = new Object[LIMIT_X][LIMIT_Y];

    public static void main(String[] args) throws InterruptedException {
        TrajectoryManager manager = new TrajectoryManager();
        Creature creature = new Creature(random.nextInt(10), random.nextInt(10));
        world[creature.getX()][creature.getY()] = creature;
        while (true) {
            Pair<Integer, Integer> trajectory = manager.generateNewTrajectory();
            if (trajectory.getKey() == 0 && trajectory.getValue() == 0) {
                continue;
            }

            world[creature.getX()][creature.getY()] = null;
            creature.setX(manager.checkXTrajectory(creature.getX(), trajectory.getKey()));
            creature.setY(manager.checkYTrajectory(creature.getY(), trajectory.getValue()));
            world[creature.getX()][creature.getY()] = creature;
            showWorld();
        }
    }

    private static void showWorld() throws InterruptedException {
        for (Object[] subWorld : world) {
            for (Object object : subWorld) {
                if (object == null) {
                    System.out.print("[ ]");
                } else {
                    System.out.print("[x]");
                }
            }

            System.out.println();
        }

        System.out.println();
        Thread.sleep(1000);
    }


}
